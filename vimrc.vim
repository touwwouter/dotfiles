set nocompatible
filetype off

" Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'flazz/vim-colorschemes'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'powerline/fonts'
Plugin 'kien/ctrlp.vim'
Plugin 'L9'
Plugin 'vim-scripts/kwbdi.vim'
Plugin 'vim-scripts/VisIncr'
Plugin 'Valloric/YouCompleteMe'
Plugin 'junegunn/vim-easy-align'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'

call vundle#end()

"-----------------------------------------------------------------------------
" General
"-----------------------------------------------------------------------------

syntax on
filetype plugin indent on
let mapleader=","
set number
set encoding=utf-8
set list listchars=tab:→\ ,trail:·
set nowrap
set cpoptions+=$
set tabstop=4
set shiftwidth=4
set expandtab
set showcmd
set hlsearch
set ignorecase

"-----------------------------------------------------------------------------
" Formattings
"-----------------------------------------------------------------------------

" t = auto wrap using textwidth
set formatoptions+=tc
set textwidth=79

" Allow modelines
set modeline

"-----------------------------------------------------------------------------
" GUI
"-----------------------------------------------------------------------------

" Remove gvim toolbar, scrollbars, and menu
set guioptions=ac

" Set the default theme
if has("gui_running")
    colorscheme solarized
    " Set the theme type depending on the time of day
    if strftime("%H") < 17
      set background=light
    else
      set background=dark
    endif
else
    colorscheme moria
endif

" Change background for column > 79 chars
let &colorcolumn=join(range(78,999), ",")

"-----------------------------------------------------------------------------
" Folding
"-----------------------------------------------------------------------------

" Use syntax folding method
set fdm=syntax

" Save folds when saving a buffer
"au BufWinLeave * mkview

" Load folds when opening a file
"au BufWinEnter * silent loadview

"-----------------------------------------------------------------------------
" Plugin Settings
"-----------------------------------------------------------------------------

" powerline
set laststatus=2
set t_Co=256
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ 10

" airline
let g:airline_powerline_fonts=1
let g:airline_theme='light'
let g:airline#extensions#syntastic#enabled=1
let g:airline_detect_modified=1
let g:airline_enable_branch=1

" ctrlp
let g:ctrlp_custom_ignore = {
    \ 'file': '\v(\.class)$'}
let g:ctrlp_working_path_mode=''

" syntastic
let g:syntastic_python_checkers=['flake8', 'python']
