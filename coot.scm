;;;
;;; ~/.coot file
;;;

;;; Symmetry
(set-show-symmetry-master 1)
(set-symmetry-whole-chain 0 1)
(set-show-unit-cells-all 1)

;;; Maps and bonds
(set-colour-map-rotation-on-read-pdb-c-only-flag 1)
(set-map-radius 20.00)
(set-smooth-scroll-flag 0)
(set-recentre-on-read-pdb 0)
(set-font-size 5)
(set-default-bond-thickness 7)
(set-show-environment-distances 1)
(set-show-environment-distances-bumps 1)
(set-show-environment-distances-h-bonds 1)
(add-key-binding "Hydrogens off" "(" 
                 (lambda ()
                   (set-draw-hydrogens 0 0)))
(add-key-binding "Hydrogens on" ")" 
                 (lambda ()
                   (set-draw-hydrogens 0 1)))

;;; Graphics
(set-graphics-window-size  1000 1000)
(set-do-anti-aliasing 1)
(show-modelling-toolbar)

;;; Pictures
(set! coot-png-display-program "ristretto")

;;; Python scripts
(set-prefer-python)

;;;
;;; Modelling and building
;;;

(add-key-binding "Add Water" "w" (lambda () (place-typed-atom-at-pointer "Water")))
(set-find-hydrogen-torsion 1)

;;;Regularization and Real Space Refinement
(define *sphere-refine-radius* 3.5)
(add-key-binding "Refine residues in a sphere" "R"
                 (lambda ()
                   (using-active-atom
                     (let* ((rc-spec (list aa-chain-id aa-res-no aa-ins-code))
                            (ls (residues-near-residue aa-imol rc-spec *sphere-refine-radius*)))
                       (refine-residues aa-imol (cons rc-spec ls))))))

(add-key-binding "Refine Active Residue" "r" (lambda () (manual-refine-residues 0)))
(add-key-binding "Refine Active Residue AA" "x" (lambda () (refine-active-residue)))
(add-key-binding "Triple Refine" "t" (lambda () (manual-refine-residues 1)))
(add-key-binding "Triple Refine AA" "h" (lambda () (refine-active-residue-triple)))

(add-planar-peptide-restraints)

;;; Rotamers
(add-key-binding "Autofit Rotamer" "j" (lambda () (auto-fit-rotamer-active-residue)))

;;; Peptide flips
(add-key-binding "Pepflip" "q" (lambda () (pepflip-active-residue)))

;;; Flip ligand
(add-key-binding "Eigen-flip Ligand" "e" (lambda() (flip-active-ligand)))

;;; Terminal residues
(add-key-binding "Add terminal residue" "y" (lambda () 
                                              (let ((active-atom (active-residue)))
                                                (if (not active-atom)
                                                  (format #t "No active atom~%")
                                                  (let ((imol      (list-ref active-atom 0))
                                                        (chain-id  (list-ref active-atom 1))
                                                        (res-no    (list-ref active-atom 2))
                                                        (ins-code  (list-ref active-atom 3))
                                                        (atom-name (list-ref active-atom 4))
                                                        (alt-conf  (list-ref active-atom 5)))
                                                    (add-terminal-residue imol chain-id res-no "auto" 1))))))

;;;
;;; Rotamer functions
;;;
(add-key-binding "Fill Partial" "k" (lambda ()
                                      (let ((active-atom (active-residue)))
                                        (if (not active-atom)
                                          (format #t "No active atom~%")
                                          (let ((imol      (list-ref active-atom 0))
                                                (chain-id  (list-ref active-atom 1))
                                                (res-no    (list-ref active-atom 2))
                                                (ins-code  (list-ref active-atom 3))
                                                (atom-name (list-ref active-atom 4))
                                                (alt-conf  (list-ref active-atom 5)))
                                            (fill-partial-residue imol chain-id res-no ins-code))))))

(add-key-binding "Rotamer name in Status Bar" "~" 
                 (lambda ()
                   (let ((active-atom (active-residue)))
                     (if (not active-residue)
                       (add-status-bar-text "No residue found")
                       (let ((imol      (list-ref active-atom 0))
                             (chain-id  (list-ref active-atom 1))
                             (res-no    (list-ref active-atom 2))
                             (ins-code  (list-ref active-atom 3))
                             (atom-name (list-ref active-atom 4))
                             (alt-conf  (list-ref active-atom 5)))
                         (let ((name (get-rotamer-name imol chain-id res-no ins-code)))
                           (if (not name)
                             (add-status-bar-text "No name found")
                             (if (string=? "" name)
                               (add-status-bar-text "No name for this")
                               (add-status-bar-text (string-append "Rotamer name: " name))))))))))

(set-rotamer-lowest-probability 0.5)

;;;
;;; NCS
;;;
(add-key-binding "Toggle Ghosts" ":" 
                 (lambda () 
                   (let ((keyboard-ghosts-mol 
                           (let ((ls (model-molecule-list)))
                             (let loop ((ls ls))
                               (cond
                                 ((null? ls) -1)
                                 ((ncs-ghosts (car ls))
                                  (car ls))
                                 (else
                                   (loop (cdr ls))))))))
                     (if (= (draw-ncs-ghosts-state keyboard-ghosts-mol) 0)
                       (set-draw-ncs-ghosts keyboard-ghosts-mol 1)
                       (set-draw-ncs-ghosts keyboard-ghosts-mol 0)))))

;;;
;;; Occupancy
;;;
(define (low-occ-gui imol occ-threshold)
  (interesting-residues-gui 
    imol
    "Residues with low occupancy..."
    (residues-matching-criteria
      imol 
      (lambda (chain-id res-no ins-code res-serial-no)

        (let ((atom-ls (residue-info imol chain-id res-no ins-code)))

          ;; return #f if there are no atoms with alt-confs, else return
          ;; a list of the residue's spec (chain-id resno ins-code)
          ;; 
          (let g ((atom-ls atom-ls))
            (cond 
              ((null? atom-ls) #f)
              (else 
                (let* ((atom (car atom-ls))
                       (occ (car (car (cdr atom)))))
                  (if (< occ occ-threshold)
                    #t
                    (g (cdr atom-ls))))))))))))

(let ((menu (coot-menubar-menu "Extras")))
  (add-simple-coot-menu-menuitem
    menu "Residues with low occupancy..."
    (lambda ()
      (generic-chooser-and-entry 
        "Molecule for low occupancy analysis:"
        "Occupancy threshold"
        "0.99"
        (lambda (imol text)
          (let ((n (string->number text)))
            (if (number? n)
              (low-occ-gui imol n))))))))

;;; Molprobity interface
(set-do-probe-dots-on-rotamers-and-chis 1)
(set-do-probe-dots-post-refine 1)

;;; Map navigation
(add-key-binding "Go To Blob" "g" (lambda () (blob-under-pointer-to-screen-centre)))
